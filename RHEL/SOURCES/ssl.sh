#!/bin/sh

DIR="/opt/ifoodorder/menu-service";
IP=$(/sbin/ip -o -4 addr list eth0 | awk '{print $4}' | cut -d/ -f1)

vaultbot --logfile=/var/log/ifoodorder/menu-service/ssl.log \
	--vault_addr=https://vault.service.consul:8200 \
	--vault_auth_method=approle \
	--vault_app_role_role_id=$(cat /etc/ifoodorder/menu-service/secrets/spring.cloud.vault.app-role.role-id) \
	--vault_app_role_secret_id=$(cat /etc/ifoodorder/menu-service/secrets/spring.cloud.vault.app-role.secret-id) \
	--pki_mount=pki_int \
	--pki_role_name=menu-cert \
	--pki_common_name=menu.service.consul \
	--pki_alt_names=_menu._tcp.service.consul,localhost \
	--pki_ttl=24h \
	--pki_renew_time=4h \
	--pki_ip_sans=127.0.0.1,${IP} \
	--pki_pkcs12_path=${DIR}/keystore.pkcs12 \
	--pki_pkcs12_password=ChangeIt \
	--pki_pkcs12_umask="644" \
	--renew_hook="sudo systemctl restart menu-service" \
	-y