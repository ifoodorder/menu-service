%define branchname %{?_branchname}%{?!_branchname:%{nil}}
%define mvnversion %{?_mvnversion}%{?!_mvnversion:0}
%define releaseversion %{?_releaseversion}%{?!_releaseversion:0}
Name: menu-service
Version: %{mvnversion}%{branchname}
Release: %{releaseversion}
License: Proprietary
Requires: vaultbot, java
BuildArch: noarch
Group: system
Summary: iFoodOrder Menu service
BuildRoot: ~/rpmbuild/

Source0: menu-service.service
Source1: menu-service_ssl.service
Source2: menu-service_ssl.timer
Source3: bootstrap.yml
Source4: *.jar
Source5: ssl.sh
Source6: menu-service

%description

%prep
# nothing
exit 0

%build
# built by CI

%pre
# create group and user if they doesn't exist
getent group menu-service >/dev/null || groupadd -r menu-service
getent passwd menu-service >/dev/null || \
    useradd -r -g menu-service -d /opt/ifoodorder/menu-service -s /sbin/nologin \
    -c "iFoodOrder service operator" menu-service
exit 0

%install
# clean
rm -rf %{buildroot}
mkdir -p %{buildroot}/

# service file
mkdir -p %{buildroot}/%{_sysconfdir}/systemd/system/
cp %{SOURCE0} %{buildroot}/%{_sysconfdir}/systemd/system/menu-service.service
cp %{SOURCE1} %{buildroot}/%{_sysconfdir}/systemd/system/menu-service_ssl.service
cp %{SOURCE2} %{buildroot}/%{_sysconfdir}/systemd/system/menu-service_ssl.timer

# config
mkdir -p %{buildroot}/%{_sysconfdir}/ifoodorder/menu-service/secrets/
cp %{SOURCE3} %{buildroot}/%{_sysconfdir}/ifoodorder/menu-service/

# runnable
mkdir -p %{buildroot}/opt/ifoodorder/menu-service
cp %{SOURCE4} %{buildroot}/opt/ifoodorder/menu-service
cp %{SOURCE5} %{buildroot}/opt/ifoodorder/menu-service

# log dir
mkdir -p %{buildroot}/var/log/ifoodorder/menu-service/

#enable self restart
mkdir -p %{buildroot}/%{_sysconfdir}/sudoers.d/
cp %{SOURCE6} %{buildroot}/%{_sysconfdir}/sudoers.d/menu-service

%clean
rm -rf %{buildroot}

%files
%defattr(644, menu-service, menu-service, -)
# service file
%attr(-, root, root) %{_sysconfdir}/systemd/system/menu-service.service
%attr(-, root, root) %{_sysconfdir}/systemd/system/menu-service_ssl.service
%attr(-, root, root) %{_sysconfdir}/systemd/system/menu-service_ssl.timer
%attr(-, root, root) %{_sysconfdir}/sudoers.d/menu-service

# config
%attr(755, -, -) %{_sysconfdir}/ifoodorder/menu-service/
%attr(700, -, -) %{_sysconfdir}/ifoodorder/menu-service/secrets/
%config %{_sysconfdir}/ifoodorder/menu-service/bootstrap.yml

# runnable
%attr(755, -, -) /opt/ifoodorder/menu-service
%attr(744, -, -) /opt/ifoodorder/menu-service/menu-service.jar
%attr(744, -, -) /opt/ifoodorder/menu-service/ssl.sh

# log
%attr(755, -, -) /var/log/ifoodorder/menu-service/
%changelog