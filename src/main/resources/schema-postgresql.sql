DROP TABLE IF EXISTS menu, category, item, alergen, item_alergen_xref CASCADE;

CREATE TABLE IF NOT EXISTS menu (
    id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
	title TEXT NOT NULL,
	info TEXT,
	enabled BOOLEAN NOT NULL,
	restaurant_id UUID NOT NULL
);

CREATE TABLE IF NOT EXISTS category (
    id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
	name TEXT,
	description TEXT,
	enabled BOOLEAN NOT NULL,
	menu_id UUID NOT NULL
);

CREATE TABLE IF NOT EXISTS item (
    id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
	name TEXT NOT NULL,
	price TEXT NOT NULL,
	enabled BOOLEAN NOT NULL,
	description TEXT,
	image TEXT,
	category_id UUID NOT NULL
);

CREATE TABLE IF NOT EXISTS alergen (
    id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
	name TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS item_alergen_xref (
    id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
	item_id UUID NOT NULL, 
	alergen_id UUID NOT NULL
);

-- category FK
ALTER TABLE category ADD FOREIGN KEY (menu_id) REFERENCES menu(id);

-- item FK
ALTER TABLE item ADD FOREIGN KEY (category_id) REFERENCES category(id);

-- item_alergen_xref FK
ALTER TABLE item_alergen_xref ADD FOREIGN KEY (item_id) REFERENCES item(id);
ALTER TABLE item_alergen_xref ADD FOREIGN KEY (alergen_id) REFERENCES alergen(id);