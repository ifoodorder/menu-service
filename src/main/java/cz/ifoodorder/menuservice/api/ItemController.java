package cz.ifoodorder.menuservice.api;

import java.util.UUID;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cz.ifoodorder.menuservice.api.pojo.ApiItem;
import cz.ifoodorder.menuservice.api.pojo.mappers.ItemMapper;
import cz.ifoodorder.menuservice.db.dto.ItemDto;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/menu/{restaurant}/items")
@Slf4j
public class ItemController {
	private final ItemDto itemDto;
	private final ItemMapper itemMapper;
	
	public ItemController(
			ItemDto itemDto,
			ItemMapper itemMapper) {
		this.itemDto = itemDto;
		this.itemMapper = itemMapper;
	}
	
	@GetMapping("/{item}")
	public ApiItem getItem(@PathVariable(name = "item") UUID itemId) {
		log.info("itemId: {}", itemId);
		return itemMapper.mapToApi(itemDto.findById(itemId).orElseThrow());
	}
}
