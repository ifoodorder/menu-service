package cz.ifoodorder.menuservice.api;

import java.util.Collection;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cz.ifoodorder.menuservice.api.pojo.ApiAlergen;
import cz.ifoodorder.menuservice.api.pojo.mappers.AlergenMapper;
import cz.ifoodorder.menuservice.db.dto.AlergenDto;

@RestController
@RequestMapping("/api/menu/{restaurant}/alergens")
public class AlergenController {
	private final AlergenDto alergenDto;
	private final AlergenMapper alergenMapper;
	public AlergenController(
			AlergenDto alergenDto,
			AlergenMapper alergenMapper) {
		this.alergenDto = alergenDto;
		this.alergenMapper = alergenMapper;
	}
	
	@GetMapping()
	public Collection<ApiAlergen> getAlergens() {
		return this.alergenMapper.mapToApi(alergenDto.findAll());
	}

}
