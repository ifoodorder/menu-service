package cz.ifoodorder.menuservice.api;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cz.ifoodorder.menuservice.api.pojo.ApiMenu;
import cz.ifoodorder.menuservice.api.pojo.mappers.MenuMapper;
import cz.ifoodorder.menuservice.db.dto.MenuDto;
import cz.ifoodorder.menuservice.db.pojo.Menu;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/menu")
@Slf4j
public class MenuController {
	
	private final MenuDto menuDto;
	private final MenuMapper menuMapper;
	private final RestaurantService restaurantService;
	
	public MenuController(MenuDto menuDto, 
			MenuMapper menuMapper,
			RestaurantService restaurantService) {
		this.menuDto = menuDto;
		this.menuMapper = menuMapper;
		this.restaurantService = restaurantService;
	}

	/**
	 * Only gets the enabled one for the selected restaurant as {@link #setMenu} disables the last enabled one and sets the new one as enabled.
	 * 
	 * @param restaurant
	 * @return
	 */
	@GetMapping("/{restaurant}")
	public Optional<ApiMenu> getMenu(@PathVariable String restaurant, Principal principal) {
		log.debug("Principal is {}", principal);
		Optional<Menu> menu = menuDto.findOneByRestaurantIdAndEnabled(UUID.fromString(restaurant), true);
		var optionalApiResponse = menu.map(menuMapper::mapToApi);
		if(optionalApiResponse.isEmpty()) {
			log.debug("No menu found");
			return Optional.empty();
		}
		
		var apiResponse = optionalApiResponse.get();
		
		UUID restaurantId = UUID.fromString(restaurant);
		log.debug("\nRestaurant ID: {}\nPrincipal ID:  {}", restaurantId, principal != null ? principal.getName() : null);
		
		var viewRestaurant = restaurantService.getRestaurantById(restaurantId);
		if(principal != null && 
				UUID.fromString(principal.getName()).equals(viewRestaurant.getUserId())) {
			log.debug("Found menu {} for restaurant owner {}", apiResponse.getId(), restaurantId);
			return Optional.of(apiResponse);
		}
		
		log.debug("Found menu {}", apiResponse.getId());
		
		var categories = apiResponse.getCategories().stream().filter(x -> x.isEnabled()).collect(Collectors.toList());
		apiResponse.setCategories(categories);
		categories.forEach(x -> {
			var items = x.getItems().stream().filter(y -> y.isEnabled()).collect(Collectors.toList());
			x.setItems(items);
		});
		
		
		return Optional.of(apiResponse);
	}
	
	/**
	 * If new menu is enabled, disables previous enabled menu to avoid conflits and sets the new one as enabled.
	 * Otherwise just adds it to DB
	 * 
	 * @param restaurant
	 * @param menuRequest
	 * @return
	 */
	@PostMapping("/{restaurant}")
	public void setMenu(@PathVariable String restaurant, @RequestBody ApiMenu menuRequest) {
		log.info("{}", menuRequest);
		
		final UUID restaurantId = UUID.fromString(restaurant);		
		final List<Menu> menus = new ArrayList<>();
		
		Optional<Menu> oldMenu = menuDto.findOneByRestaurantIdAndEnabled(restaurantId, true);
		Menu newMenu = menuMapper.mapToDb(menuRequest);
		
		newMenu.setRestaurantId(restaurantId);
		menus.add(newMenu);
		
		if(newMenu.isEnabled() && oldMenu.isPresent()) {
			var menu = oldMenu.get();
			menu.setEnabled(false);
			menus.add(menu);
		}
		
		menuDto.saveAll(menus);
	}
	

}
