package cz.ifoodorder.menuservice.api.pojo.mappers;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import cz.ifoodorder.menuservice.api.pojo.ApiCategory;
import cz.ifoodorder.menuservice.db.pojo.Category;
import cz.ifoodorder.menuservice.db.pojo.Menu;


@Component
public class CategoryMapper {
	
	private final ItemMapper itemMapper;
	
	public CategoryMapper(ItemMapper itemMapper) {
		this.itemMapper = itemMapper;
	}
	
	public Category mapToDb(ApiCategory viewCategory) {
		return mapToDb(viewCategory, null);
	}
	public Category mapToDb(ApiCategory viewCategory, Menu menu) {
		if(viewCategory == null) {
			return null;
		}
		var category = new Category();
		category.setDescription(viewCategory.getDescription());
		category.setEnabled(viewCategory.isEnabled());
		category.setId(viewCategory.getId());
		category.setItems(itemMapper.mapToDb(viewCategory.getItems(), category));
		category.setMenu(menu);
		category.setName(viewCategory.getName());
		return category;
	}
	
	public ApiCategory mapToApi(Category category) {
		if(category == null) {
			return null;
		}
		var apiCategory = new ApiCategory();
		apiCategory.setDescription(category.getDescription());
		apiCategory.setEnabled(category.isEnabled());
		apiCategory.setId(category.getId());
		apiCategory.setName(category.getName());
		apiCategory.setItems(itemMapper.mapToApi(category.getItems()));
		return apiCategory;
	}
	
	public Collection<Category> mapToDb(Collection<ApiCategory> viewCategory) {
		return mapToDb(viewCategory, null);
	}
	
	public Collection<Category> mapToDb(Collection<ApiCategory> viewCategory, Menu menu) {
		return viewCategory.stream().map(x -> mapToDb(x, menu)).collect(Collectors.toList());
	}

	public Collection<ApiCategory> mapToApi(Collection<Category> categories) {
		return categories.stream().map(this::mapToApi).collect(Collectors.toList());
	}
}
