package cz.ifoodorder.menuservice.api.pojo;

import java.util.Collection;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApiItem {
	private UUID id;
	private String name;
	private String price;
	private boolean enabled;
	private String description;
	private Collection<ApiAlergen> allergens;
	private String image;
}
