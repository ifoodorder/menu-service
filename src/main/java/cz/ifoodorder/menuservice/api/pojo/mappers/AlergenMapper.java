package cz.ifoodorder.menuservice.api.pojo.mappers;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import cz.ifoodorder.menuservice.api.pojo.ApiAlergen;
import cz.ifoodorder.menuservice.db.dto.AlergenDto;
import cz.ifoodorder.menuservice.db.pojo.Alergen;

@Component
public class AlergenMapper {
	private final AlergenDto alergenDto;
	AlergenMapper(AlergenDto alergenDto) {
		this.alergenDto = alergenDto;
	}
	
	public Alergen mapToDb(ApiAlergen viewAlergen) {
		if(viewAlergen == null) {
			return null;
		}
		return alergenDto.findOneByName(viewAlergen.getName()).orElseThrow();
	}
	public ApiAlergen mapToApi(Alergen alergen) {
		if(alergen == null) {
			return null;
		}
		var apiAlergen = new ApiAlergen();
		apiAlergen.setName(alergen.getName());
		return apiAlergen;
	}
	
	public Collection<ApiAlergen> mapToApi(Collection<Alergen> alergen) {
		return alergen.stream().map(this::mapToApi).collect(Collectors.toList()); 
	}
	
	public Collection<Alergen> mapToDb(Collection<ApiAlergen> allergens) {
		return allergens.stream().map(this::mapToDb).collect(Collectors.toList());
	}
}
