package cz.ifoodorder.menuservice.api.pojo.mappers;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import cz.ifoodorder.menuservice.api.pojo.ApiItem;
import cz.ifoodorder.menuservice.db.pojo.Category;
import cz.ifoodorder.menuservice.db.pojo.Item;

@Component
public class ItemMapper {
	private final AlergenMapper alergenMapper;
	ItemMapper(AlergenMapper alergenMapper) {
		this.alergenMapper = alergenMapper;
	}
	
	public Item mapToDb(ApiItem viewItem) {
		return mapToDb(viewItem, null);
	}
	public Item mapToDb(ApiItem viewItem, Category category) {
		if(viewItem == null) {
			return null;
		}
		var item = new Item();
		item.setName(viewItem.getName());
		item.setDescription(viewItem.getDescription());
		item.setAlergens(alergenMapper.mapToDb(viewItem.getAllergens()));
		item.setEnabled(viewItem.isEnabled());
		item.setImage(viewItem.getImage());
		item.setPrice(viewItem.getPrice());
		item.setId(viewItem.getId());
		item.setCategory(category);
		return item;
	}
	
	public ApiItem mapToApi(Item item) {
		if(item == null) {
			return null;
		}
		var apiItem = new ApiItem();
		apiItem.setAllergens(alergenMapper.mapToApi(item.getAlergens()));
		apiItem.setDescription(item.getDescription());
		apiItem.setEnabled(item.isEnabled());
		apiItem.setImage(item.getImage());
		apiItem.setName(item.getName());
		apiItem.setPrice(item.getPrice());
		return apiItem;
	}
	public Collection<ApiItem> mapToApi(Collection<Item> items) {
		return items.stream().map(this::mapToApi).collect(Collectors.toList());
	}

	public Collection<Item> mapToDb(Collection<ApiItem> items) {
		return items.stream().map(this::mapToDb).collect(Collectors.toList());
	}

	public Collection<Item> mapToDb(Collection<ApiItem> items, Category category) {
		return items.stream().map(x -> mapToDb(x, category)).collect(Collectors.toList());
	}
}
