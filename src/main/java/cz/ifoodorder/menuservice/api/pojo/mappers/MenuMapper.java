package cz.ifoodorder.menuservice.api.pojo.mappers;

import org.springframework.stereotype.Component;

import cz.ifoodorder.menuservice.api.pojo.ApiMenu;
import cz.ifoodorder.menuservice.db.pojo.Menu;

@Component
public class MenuMapper {
	private final CategoryMapper categoryMapper;
	public MenuMapper(CategoryMapper categoryMapper) {
		this.categoryMapper = categoryMapper;
	}
	
	public Menu mapToDb(ApiMenu viewMenu) {
		if(viewMenu == null) {
			return null;
		}
		var menu = new Menu();
		menu.setId(viewMenu.getId());
		menu.setEnabled(viewMenu.isEnabled());
		menu.setInfo(viewMenu.getInfo());
		menu.setRestaurantId(viewMenu.getRestaurantId());
		menu.setTitle(viewMenu.getTitle());
		menu.setCategories(categoryMapper.mapToDb(viewMenu.getCategories(), menu));
		return menu;
	}
	
	public ApiMenu mapToApi(Menu menu) {
		if(menu == null) {
			return null;
		}
		var apiMenu = new ApiMenu();
		apiMenu.setEnabled(menu.isEnabled());
		apiMenu.setId(menu.getId());
		apiMenu.setInfo(menu.getInfo());
		apiMenu.setRestaurantId(menu.getRestaurantId());
		apiMenu.setTitle(menu.getTitle());
		apiMenu.setCategories(categoryMapper.mapToApi(menu.getCategories()));
		return apiMenu;
	}
}
