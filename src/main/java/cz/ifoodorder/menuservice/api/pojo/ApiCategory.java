package cz.ifoodorder.menuservice.api.pojo;

import java.util.Collection;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApiCategory {	
	private UUID id;
	private String name;
	private String description;
	private Collection<ApiItem> items;
	private boolean enabled;
}
