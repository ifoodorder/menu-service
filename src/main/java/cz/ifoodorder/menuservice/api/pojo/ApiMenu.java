package cz.ifoodorder.menuservice.api.pojo;

import java.util.Collection;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApiMenu {	
	private UUID id;
	private String title;
	private String info;
	private boolean enabled;
	private UUID restaurantId;
	private Collection<ApiCategory> categories;

}
