package cz.ifoodorder.menuservice.api.pojo;

import com.fasterxml.jackson.annotation.JsonValue;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiAlergen {
	@JsonValue
	private String name;
}
