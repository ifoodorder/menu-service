package cz.ifoodorder.menuservice.restaurants;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ViewRestaurant {
	private UUID id;
	private UUID userId;
}

