package cz.ifoodorder.menuservice.db.dto;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import cz.ifoodorder.menuservice.db.pojo.Alergen;

public interface AlergenDto extends JpaRepository<Alergen, UUID> {
	Optional<Alergen> findOneByName(String name);
}
