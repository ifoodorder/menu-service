package cz.ifoodorder.menuservice.db.dto;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import cz.ifoodorder.menuservice.db.pojo.Ingredient;

public interface IngredientDto extends JpaRepository<Ingredient, UUID> {

}
