package cz.ifoodorder.menuservice.db.dto;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import cz.ifoodorder.menuservice.db.pojo.Category;

public interface CategoryDto extends JpaRepository<Category, UUID> {
	List<Category> findByMenuId(UUID menuId);
}
