package cz.ifoodorder.menuservice.db.dto;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import cz.ifoodorder.menuservice.db.pojo.Menu;

public interface MenuDto extends JpaRepository<Menu, UUID> {
	Menu findOneByRestaurantId(UUID restaurantId);
	
	Optional<Menu> findOneByRestaurantIdAndEnabled(UUID restaurantId, boolean enable);
}
