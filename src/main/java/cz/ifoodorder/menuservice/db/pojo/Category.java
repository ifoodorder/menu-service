package cz.ifoodorder.menuservice.db.pojo;

import java.util.Collection;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity(name = "category")
public class Category {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID id;
	private String name;
	private String description;
	@ManyToOne (fetch = FetchType.LAZY)
	@JoinColumn(name = "menu_id")
	private Menu menu;
	@OneToMany(mappedBy = "category", cascade = CascadeType.ALL)
	private Collection<Item> items;
	private boolean enabled;
}
