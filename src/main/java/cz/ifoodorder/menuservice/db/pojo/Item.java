package cz.ifoodorder.menuservice.db.pojo;

import java.util.Collection;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity(name = "item")
public class Item {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID id;
	private String name;
	private String price;
	private boolean enabled;
	private String description;
	@ManyToOne (fetch = FetchType.LAZY)
	@JoinColumn(name = "category_id")
	private Category category;
	@ManyToMany
	@JoinTable(
			name = "item_alergen_xref",
			joinColumns = @JoinColumn(name = "item_id", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "alergen_id", referencedColumnName = "id")
	)
	private Collection<Alergen> alergens;
	private String image;
}
