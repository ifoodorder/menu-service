package cz.ifoodorder.menuservice.db.pojo;

import java.util.Set;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity(name = "alergen")
public class Alergen {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID id;
	private String name;
	@ManyToMany(mappedBy = "alergens", fetch = FetchType.LAZY)
	@EqualsAndHashCode.Exclude
	private Set<Item> items;
}
