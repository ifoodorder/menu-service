package cz.ifoodorder.menuservice.db.pojo;

import java.util.Collection;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity(name = "menu")
public class Menu {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID id;
	private String title;
	private String info;
	private boolean enabled;
	private UUID restaurantId;
	// joins
	@OneToMany(mappedBy = "menu", cascade = CascadeType.ALL, targetEntity = Category.class)
	private Collection<Category> categories;
	
}
