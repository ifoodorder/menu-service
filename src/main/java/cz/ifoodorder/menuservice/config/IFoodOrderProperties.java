package cz.ifoodorder.menuservice.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Data
@Configuration
@ConfigurationProperties(prefix = "ifoodorder")
public class IFoodOrderProperties {
	private String restaurantUrl;
	private String menuUrl;
	private String restaurantAdminRole;
}
