FROM adoptopenjdk/openjdk11:alpine-jre
RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring
COPY target/*.jar app.jar
ENV RESTAURANT_HOST http://restaurant-service:8050
ENV MENU_HOST http://menu-service:8053
ENV KEYCLOAK_SECRET 98585d22-4e4f-4e1c-9122-9890be29bbde
ENV KEYCLOAK_HOST http://keycloak:8080
ENV POSTGRESQL_HOST jdbc:postgresql://postgresql:5432
ENTRYPOINT ["java","-Dspring.profiles.active=docker","-jar","app.jar"]