#!/bin/bash
DIR="$(dirname "$0")";
IP=$(/sbin/ip -o -4 addr list eth0 | awk '{print $4}' | cut -d/ -f1)
${DIR}/vaultbot --logfile=${DIR}/vaultbot.log \
           --vault_addr=https://vault.service.consul:8200 \
           --vault_auth_method=approle \
           --vault_app_role_role_id={{ MENU_ROLE_ID }} \
           --vault_app_role_secret_id={{ MENU_SECRET_ID }} \
           --pki_mount=pki_int \
           --pki_role_name=menu-cert \
           --pki_common_name=menu.service.consul \
           --pki_alt_names=_menu._tcp.service.consul,localhost \
           --pki_ttl=24h \
           --pki_renew_time=4h \
           --pki_ip_sans=127.0.0.1,${IP} \
           --pki_cert_path=${DIR}/cert.pem \
           --pki_cachain_path=${DIR}/cachain.pem \
           --pki_privkey_path=${DIR}/privkey.pem \
           --pki_pembundle_path=${DIR}/bundle.pem \
           -y

sleep 5;

openssl pkcs12 -export -in ${DIR}/bundle.pem \
               -inkey ${DIR}/privkey.pem \
               -name menu \
               -passout pass:ChangeIt \
               -out ${DIR}/keystore.pkcs12

chown spring:spring ${DIR}/keystore.pkcs12

systemctl restart menu-service